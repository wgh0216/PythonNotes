
> 连玉君的 Python 学习笔记 (`2019/5/22 17:02`)

## Python 学习资源汇总

### 在线网站
- [廖雪峰个人网站-Python](https://www.liaoxuefeng.com/wiki/1016959663602400)
- [RUNOOB.com](https://www.runoob.com/python3)
- [C语言中文网-Python基础教程](http://c.biancheng.net/python/)
- [Data Analysis in Python](http://www.data-analysis-in-python.org/index.html)，一个非常好的在线学习网站，重点在于数据分析、最优化等

### 包和模块

- `NumPy`    科学计算包，[Numpy手册](https://docs.scipy.org/doc/numpy-1.16.1/reference/)
- `operater` 运算符模块
- `StatsModels` see [Data Analysis in Python](http://www.data-analysis-in-python.org/index.html) >> [Econometrics](http://www.data-analysis-in-python.org/t_statsmodels.html) 


### 安装教程
- [Anaconda介绍、安装及使用教程](https://www.jianshu.com/p/62f155eb6ac5)


## 专题教程
- OLS
  - [http://www.statsmodels.org](http://www.statsmodels.org/stable/) >> [OLS](http://www.statsmodels.org/stable/examples/notebooks/generated/ols.html)
  - [OLS](https://github.com/arlionn/statsintro_python/blob/master/ipynb/11_correlationRegression.ipynb)
  - [OLS - scipy-lectures.org](https://scipy-lectures.org/packages/statistics/index.html)，图形化展示，交乘项等
- [Bootstrap](https://github.com/arlionn/statsintro_python/blob/master/ipynb/11_bootstrapping.ipynb)，太简略

